package com.example.demo;

import java.io.Serializable;

public class EchoForm implements Serializable {

	private double val1;
	private double val2;
	private double rslt;
	
	public double getVal1() {
		return val1;
	}

	public void setVal1(double val1) {
		this.val1 = val1;
	}

	public double getVal2() {
		return val2;
	}

	public void setVal2(double val2) {
		this.val2 = val2;
	}

	public void cmp_add() {
		double ans = val1 + val2;
		this.rslt = ans;
	}
	
	public void cmp_sub() {
		double ans = val1 - val2;
		this.rslt = ans;
	}
	
	public void cmp_div() {
		double ans = val1 / val2;
		this.rslt = ans;
	}
	
	public void cmp_mul() {
		double ans = val1 * val2;
		this.rslt = ans;
	}

	public double getRslt() {
		System.out.println("val1=" + this.val1);
		System.out.println("val2=" + this.val2);
		System.out.println("rslt=" + this.rslt);
		return rslt;
	}

}