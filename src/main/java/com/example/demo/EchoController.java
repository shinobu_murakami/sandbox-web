package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping

public class EchoController {

//*******************************************
    @ExceptionHandler(NullPointerException.class)
    public String NullPointerExceptionHandler() {
        System.out.println("NullPointerException occured");
        return "error";
    }
    @ExceptionHandler(IllegalArgumentException.class)
    public String IllegalArgumentExceptionHandler() {
        System.out.println("IllegalArgumentException  occured");
        return "error";
    }
    @ExceptionHandler(Throwable.class)
    public String ThrowableHandler() {
        System.out.println("Exception  occured");
        return "error";
    }

    @RequestMapping(value = "/err", method = RequestMethod.GET)
    public String errorpage() throws Exception {
        throw new Exception();
    }

//*******************************************

	@ModelAttribute   //指定したクラスにリクエストパラメータをバインド
	public EchoForm setUpEchoForm() {
		EchoForm form = new EchoForm();
		return form;
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		return "index";
	}

	@RequestMapping(value = "addition", method = RequestMethod.GET)
	public String addition(Model model) {
		return "addition";
	}

	@RequestMapping(value = "subtraction", method = RequestMethod.GET)
	public String subtraction(Model model) {
		return "subtraction";
	}

	@RequestMapping(value = "division", method = RequestMethod.GET)
	public String division(Model model) {
		return "division";
	}

	@RequestMapping(value = "multiplication", method = RequestMethod.GET)
	public String mulitplication(Model model) {
		return "multiplication";
	}

	@RequestMapping(value = "echo", method = RequestMethod.POST)
	public String echo(EchoForm form, Model model) {
		model.addAttribute("val1", form.getVal1());
		model.addAttribute("val2", form.getVal2());
		model.addAttribute("rslt", form.getRslt());
		return "echo";
	}

	@RequestMapping(value = "echo_add", method = RequestMethod.POST)
	public String echo_add(EchoForm form, Model model) {
		form.cmp_add();
		model.addAttribute("val1", form.getVal1());
		model.addAttribute("val2", form.getVal2());
		model.addAttribute("rslt", form.getRslt());
		return "echo_add";
	}

	@RequestMapping(value = "echo_sub", method = RequestMethod.POST)
	public String echo_sub(EchoForm form, Model model) {
		form.cmp_sub();
		model.addAttribute("val1", form.getVal1());
		model.addAttribute("val2", form.getVal2());
		model.addAttribute("rslt", form.getRslt());
		return "echo_sub";
	}

	@RequestMapping(value = "echo_div", method = RequestMethod.POST)
	public String echo_div(EchoForm form, Model model) {
		form.cmp_div();
		model.addAttribute("val1", form.getVal1());
		model.addAttribute("val2", form.getVal2());
		model.addAttribute("rslt", form.getRslt());
		return "echo_div";
	}

	@RequestMapping(value = "echo_mul", method = RequestMethod.POST)
	public String echo_mul(EchoForm form, Model model) {
		form.cmp_mul();
		model.addAttribute("val1", form.getVal1());
		model.addAttribute("val2", form.getVal2());
		model.addAttribute("rslt", form.getRslt());
		return "echo_mul";
	}

}

